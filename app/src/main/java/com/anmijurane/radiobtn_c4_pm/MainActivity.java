package com.anmijurane.radiobtn_c4_pm;
/**
 * El desarrollo fue en la API de Android 6.0 Mashmellow
 * @autor anmijurane
 */

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText num1, num2;
    private TextView result;
    private RadioButton btnResta, btnSuma, btnMult, btnDiv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num1 = (EditText) findViewById(R.id.Number1);
        num2 = (EditText) findViewById(R.id.Number2);
        result = (TextView) findViewById(R.id.TxtView_Result);
        btnResta = (RadioButton) findViewById(R.id.Btn_resta);
        btnSuma= (RadioButton) findViewById(R.id.Btn_suma);
        btnMult = (RadioButton) findViewById(R.id.Btn_mult);
        btnDiv = (RadioButton) findViewById(R.id.Btn_div);
        findViewById(R.id.Btn_oper).setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        double numberOne = Double.parseDouble(num1.getText().toString());
        double numberTwo = Double.parseDouble(num2.getText().toString());

        if (btnSuma.isChecked()){
            result.setText("RESULTADO: " + OperSuma(numberOne, numberTwo));
        }else if(btnResta.isChecked()){
            result.setText("RESULTADO: " + OperResta(numberOne, numberTwo));
        }else if(btnMult.isChecked()){
            result.setText("RESULTADO: " + OperMult(numberOne, numberTwo));
        }else if(btnDiv.isChecked()){
            result.setText("RESULTADO: " + OperDiv(numberOne, numberTwo));
        }
    }

    public double OperSuma(double n1, double n2){
        return n1 + n2;
    }

    public double OperResta(double n1, double n2){
        return n1 - n2;
    }

    public double OperMult(double n1, double n2){
        return n1 * n2;
    }

    public double OperDiv(double n1, double n2){
        return n1 / n2;
    }

}
